public class Main {
    private static int fibbonachi(int element){
        if (element == 0) {
            return 0;
        } else if (element == 1 || element == 2) {
            return 1;
        } 
        return fibbonachi(element-1) + fibbonachi(element-2);
    }
    public static void main (String[] args){
        System.out.println(fibbonachi(4));
    }
}


// 1e ronde fibbonachi(4) : 3 > gaat nogmaals   ; 2 = 1             --> 1 over
// 2e ronde fibbonachi(4) : 2 = 1               ; 1 = 1             --> 2 over
// bij het terug geven worden alle 1tjes bij elkaar opgeteld, eindresultaat bij fibbonachi(4) = 3